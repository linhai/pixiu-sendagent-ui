Ext.define('PixiuSendagentUi.view.main.FileTasksModel', {
  extend: 'Ext.app.ViewModel',

  requires: [
    'PixiuSendagentUi.model.FileTasks'
  ],

  alias:'viewmodel.file_tasks',

  stores: {
    file_task: {
      model: 'PixiuSendagentUi.model.FileTasks',
      pageSize: 20,
      autoLoad: true,

      proxy: {
        type: 'ajax',
        url: 'http://localhsot:30600/v1/file-transfer-tasks',

        reader: {
          type: 'json',
          rootProperty: 'file_transfer_tasks',
          totalProperty: 'meta.total'
        }
      }
    }
  },
});
