/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('PixiuSendagentUi.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'PixiuSendagentUi'
    }

    //TODO - add data, formulas and/or methods to support your view
});
