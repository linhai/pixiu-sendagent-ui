Ext.define('PixiuSendagentUi.view.main.DatabasesTasksModel', {
  extend: 'Ext.app.ViewModel',

  requires: [
    'PixiuSendagentUi.model.DatabasesTasks'
  ],

  alias: 'viewmodel.db_tasks',

  stores: {
    databases_tasks: {
      model: 'PixiuSendagentUi.model.DatabasesTasks',
      pageSize: 20,
      autoLoad: true,

      proxy: {
        type: 'ajax',
        url: 'http://localhost:30600/v1/db-transfer-tasks',

        reader: {
          type: 'json',
          rootProperty: 'db_transfer_tasks',
          totalProperty: 'meta.total'
        }
      }
    }
  }
});
