#!/bin/bash

export _INFO_FLAG="< INFO >"

ECHO_INFO()
{
    if [ X"$1" == X"-n" ]; then
        shift 1
        if [ X"${TERM}" == X"xterm" -o X"${TERM}" == X"linux" -o X"${TERM}" == X"pcconsole" ]; then
            echo -ne "\033[42m${_INFO_FLAG}\033[0m $@"
        else
            echo -ne "${_INFO_FLAG} $@"
        fi
    else
        if [ X"${TERM}" == X"xterm" -o X"${TERM}" == X"linux" -o X"${TERM}" == X"pcconsole" ]; then
            echo -e "\033[42m${_INFO_FLAG}\033[0m $@"
        else
            echo -e "${_INFO_FLAG} $@"
        fi
    fi
}

ECHO_INFO "==========================================================="
ECHO_INFO "="
ECHO_INFO "= 开始打包 Pixiu Sendagent UI"
ECHO_INFO "="
ECHO_INFO "==========================================================="

cd `dirname $0`
BASE_PATH=`pwd`

NWJS_HOME=/home/dev/Apps/nwjs-v0.12.0-linux-x64

TMP_DIR=/tmp/PixiuSendagentUI
rm -rf $TMP_DIR
mkdir $TMP_DIR

TMP_FILE=$TMP_DIR/PixiuSendagentUI
TMP_NW_FILE=/tmp/PixiuSendagentUI.nw
rm -rf $TMP_NW_FILE

TARGET_FILE=$BASE_PATH/build/PixiuSendagentUI.tar.gz
rm -f $TARGET_FILE

cd $BASE_PATH/build/production/PixiuSendagentUi
zip -r $TMP_NW_FILE *
cat $NWJS_HOME/nw $TMP_NW_FILE > $TMP_FILE
chmod +x $TMP_FILE

cp -rf $NWJS_HOME/* $TMP_DIR

cd $TMP_DIR/..
tar zcvf $TARGET_FILE PixiuSendagentUI

ECHO_INFO "打包 Pixiu Sendagent UI 完成"
